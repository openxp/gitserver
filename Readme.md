# Local git server

Run following

```bash
docker-compose build
docker-compose up -d
docker-compose exec git-server mkrepo my-repo
```

Then add your local gitserver as a remote

```bash
git remote add local http://localhost:8007/my-repo.git
git push local --all
```

Your local repo can be reached from within ArgoCD with the URL `http://host.docker.internal:8007/my-repo.git`. You may have to add 127.0.0.1 host.docker.internal to `/etc/hosts` file

## Links used

* [Setup git server locally](https://pcarion.com/blog/git-server-locally)
* [Setup git server with docker-compose](https://linuxhint.com/setup_git_http_server_docker/)
  